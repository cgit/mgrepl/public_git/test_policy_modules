#!/bin/sh -e

DIRNAME=`dirname $0`
cd $DIRNAME
USAGE="$0 [ --update ]"
if [ `id -u` != 0 ]; then
echo 'You must be root to run this script'
exit 1
fi

echo "Building and Loading Policy"
set -x
make -f /usr/share/selinux/devel/Makefile
/usr/sbin/semodule -i myjabberd.pp
/sbin/restorecon -F -R -v /usr/bin/router /usr/bin/sm /usr/bin/c2s /usr/bin/s2s /var/lib/jabberd
/usr/sbin/semanage port -a -t jabber_router_port_t -p tcp 5347 2> /dev/null

