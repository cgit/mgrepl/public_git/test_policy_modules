## <summary>Passenger policy</summary>

######################################
## <summary>
##      Execute passenger in the passenger domain.
## </summary>
## <param name="domain">
##      <summary>
##      The type of the process performing this action.
##      </summary>
## </param>
#
interface(`passenger_domtrans',`
        gen_require(`
                type passenger_t;
        ')

	allow $1 self:capability { fowner fsetid };

	allow $1 passenger_t:process signal;

	domtrans_pattern($1, passenger_exec_t, passenger_t)
	allow $1 passenger_t:unix_stream_socket { read write shutdown };
	allow passenger_t $1:unix_stream_socket { read write };
')

######################################
## <summary>
##      Manage passenger state content.
## </summary>
## <param name="domain">
##      <summary>
##      Domain allowed access.
##      </summary>
## </param>
#
interface(`passenger_manage_state_content',`
        gen_require(`
                type passenger_state_t;
        ')

        files_search_pids($1)
	manage_dirs_pattern($1, passenger_state_t, passenger_state_t)
        manage_files_pattern($1, passenger_state_t, passenger_state_t)
	manage_fifo_files_pattern($1, passenger_state_t, passenger_state_t)
	manage_sock_files_pattern($1, passenger_state_t, passenger_state_t)
')

########################################
## <summary>
##      Read passenger lib files
## </summary>
## <param name="domain">
##      <summary>
##      Domain to not audit.
##      </summary>
## </param>
#
interface(`passenger_read_lib_files',`
        gen_require(`
                type passenger_var_lib_t;
        ')

	files_search_var_lib($1)
        read_files_pattern($1, passenger_var_lib_t, passenger_var_lib_t)
        read_lnk_files_pattern($1, passenger_var_lib_t, passenger_var_lib_t)
')

