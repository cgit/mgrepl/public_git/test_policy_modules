#!/bin/bash

DIRNAME=`dirname $0`
cd $DIRNAME
USAGE="$0 [ --update ]"
if [ `id -u` != 0 ]; then
echo 'You must be root to run this script'
exit 1
fi

/usr/sbin/semodule -l | grep mod_passanger > /dev/null

if [ $? -eq 0 ];
    then
        /usr/sbin/semodule -r mod_passanger.pp 2> /dev/null
fi

echo "Building and Loading Policy"

set -x
make -f /usr/share/selinux/devel/Makefile
/usr/sbin/semodule -i passenger.pp

/sbin/restorecon -F -R -v /var/lib/passenger /var/run/passenger
/sbin/restorecon -F -R -v /usr/lib/ruby/gems/1.8/gems/passenger-2.2.15/ext/apache2/ApplicationPoolServerExecutable
